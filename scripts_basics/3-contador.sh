#! /bin/bash
# @JennerSarmiento
# Febrer 2022
# Descripció: contador fins a 0
#--------------------------------
ERROR_NARGS=1

# Control N.Args
if [ $# -ne 1 ];
then
  echo "Error: N.Args != de 0 "
  echo "Usage: $0 num"
  exit $ERROR_NARGS
fi

# Script
MAX=$1
num=0
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done 
exit 0
