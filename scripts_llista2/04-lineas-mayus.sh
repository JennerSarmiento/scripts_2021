#! /bin/bash 
# @JennerSarmiento
# Març de 2022
# Descripció: Enumerar stdin per linies i en mayuscules
#----------------------------------------------------------
num=1
while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))
done
