#! /bin/bash
# @JennerSarmiento
# Març de 2022
# Descripció: mostrar per stdin en format T. Snyder
#-----------------------------------------------------
#Xixa
while read -r line
do 
  echo "$line" | sed -r 's/^([A-Z]{1}).* /\1. /'
done
