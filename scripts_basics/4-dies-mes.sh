#! /bin/bash
# @JennerSarmiento
# Febrer 2022
# Descripcio: Rebre 1 o +1 arg i dir quants dies son 
#-------------------------------------------------
ERROR_NARGS=1
ERROR_NUM=2
#Control N.Args
if [ $# -eq 0 ];
then
  echo "Error: Numero d'arguments Erroni"
  echo "Usage: $0 [1-12] [1-12]"
  exit $ERROR_NARGS
fi

#Script 
for arg in $*
do
  if [ $arg -lt "1" -o $arg -gt "12" ];
  then
     echo "Error. El número ha d'estar comprés entre 1 i 12."
     echo "Usage: $0 [1-12] ..."
     exit $ERROR_NUM
  else
    case $arg in
    1|3|5|7|8|10|12)
      echo "$arg té 31 dies.";;
    2)
      echo "$arg té 28/29 dies.";;
    4|6|9|11)
      echo "$arg té 30 dies.";;
    esac
  fi
done
exit 0
