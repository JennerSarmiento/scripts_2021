#! /bin/bash
# @JennerSarmiento
# Març de 2022 
# Descripció: Processar matricules en format (9999-AAA). Stdout les valides i les erronèas per stderr
#-------------------------------------------
ERROR_NARGS=1
STATUS=0

#Validar Numero de arguments

if [ $# -eq 0 ]
then
  echo "Numero de arguments erronis."
  echo "USAGE: $0 args"
  exit $ERROR_NARGS
fi

# Xixa
for arg in $*
do
  echo $arg | grep -E "^[0-9]{4}-[A-Z]{3}$" 2>/dev/null
  if [ $? -ne 0 ]
  then
    echo "$arg" >> /dev/stderr
    STATUS=$((STATUS+1))
  fi
done
exit $STATUS
