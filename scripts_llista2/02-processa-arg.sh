#! /bin/bash
# @JennerSarmiento
# Març de 2022
# Descripció: Processa arg i compta quines són de 3 o + ch.
#-----------------------------------------------------------
BASE=0
ERROR_NARGS=1
#Error D'arguments
if [ $# -eq 0 ]
then
  echo "Error N.arg"
  echo "Usage: $0 arg1 arg2 arg3"
  exit $ERROR_NARGS
fi
#Xixa
COMPTADOR=0
for arg in $*
do
  echo $arg | grep -E ".{3,}"
  if [ $? = 0 ]
  then
    ((COMPTADOR++)) 
  fi
done
echo "Hi ha $COMPTADOR arguments que tenen 3 o + characters."
exit $BASE
