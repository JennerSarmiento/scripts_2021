#! /bin/bash
# @JennerSarmiento 2021-2022
# ASIX M01-ISO
# Exemple de Funcions

function hola(){
  echo "hola"
  return 0
}

function dia(){
  date
  return 0
}

function suma(){
  echo $(($1+$2))
  return 0
}

function all(){
  hola
  echo "Avui som $(dia)."
  suma 6 9
}

function fsize(){
#Donat un login calcular du del home del user (en el /etc/passwd)
  
  login=$1
  linea=$(grep "^$login:" /etc/passwd)
  
  if [ -z "$linea" ]; then
    echo "ERROR: user $login no existeix"
    return 1
  fi
  
  dirHome=$(echo $linea | cut -d: -f6 )

  du -hs $dirHome 2>/dev/null
  return 0
} 

function loginargs(){
#rep logins i per cada login es mostra
#l'ocupacio del disc del home de l'usuari fent use del fsize.
  #1)Validar almenys que es rep 1 login
  
  if [ $# -eq 0 ]; then
    echo "Error: No has posat arg."
    echo "USAGE: Function arg arg..."
    return 1
  fi
  #2)iterar per cada login (for)
  
  for login in $*
  do
    fsize $login
  done
}

function loginfile(){
  #Rep com arg un file que conte un login per linia. Mostrar ocupacio de disc de cada usuari usant fsize. 
  #Verificar que es rep un arg i que es un regular file

  if [ $# -ne 1 ]; then
    echo "Error: La funcio funciona amb un sol arg"
    echo "USAGE: Function r-file"
    return 1
  fi
  if [ ! -f "$1" ]; then
    echo "Error: $1 no es un regular file"
    echo "Usage: Function r-file"
    return 1
  fi
  while read -r line
  do
    fsize $line
  done < $1
}   

function loginboth(){
#USAGE loginboth file --> Procesa file o stdin, mostra fsize dels users.
  fileIN=/dev/stdin
  
  if [ $# -eq 1 ]; then
    fileIN=$1
  fi 
  while read -r line
  do 
    fsize $line
  done <$fileIN
}

function grepgid(){
# GID.
# Validar arg i gid valid 
# Retorna la llista de logins que tenen aquest grup com a grup principal

  if [ $# -ne 1 ]; then
    echo "ERROR: No hi ha args."
    echo "USAGE: function GID "
    return 1  
  fi

  gid=$1
  grep -q "^[^:]*:[^:]*:$gid:" /etc/group

  if [ $? -ne 0 ]; then
    echo "ERROR: GID no existeix"
    return 2
  fi
  cut -d: -f1,4 /etc/passwd | grep ":$gid$"
}     

function gidsize(){
#Donat un GID com a arg, mostrar per a cada user que pertany aquest grup l'ocupacio de disc del seu home.
  gid=$1
  llista_users=$(grepgid $gid| cut -d: -f1)
  for user in $llista_users
  do 
   fsize $user
  done 
}


function allgidsize(){

  all_gids=$(cut -d: -f4 /etc/passwd | sort -nu)
  for gid in $all_gids
  do 
    echo ""
    echo "GID: $gid --------------"
    echo ""
    gidsize $gid
  done

}

function filterGroup(){
#llistar linies del /etc/passwd dels grup (0-100)
  while read -r line
  do
    gid=$(echo $line | cut -d: -f4) 
  
    if [ $gid -ge 0 -a $gid -le 100 ]; then
      echo $line
    fi
  done < /etc/passwd
} 


