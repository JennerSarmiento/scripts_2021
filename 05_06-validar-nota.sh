#! /bin/bash
# @JennerSarmiento 
# Any:Curs 2021-2022
# Febrer 2022
# Validar nota: suspès, aprovat
# -------------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi	
# 2) Validar rang nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error nota $1 no valida."
  echo "La nota pren valors de 0 a 10."
  echo "USAGE: $0 nota"
  exit $ERR_NOTA
fi
	
#xixa
if [ $1 -lt 5 ]
then
  echo "Suspés"
  exit 0
elif [ $1 -eq 5 ]
then
  echo "Aprovat Pelat"
  exit 0
elif [ $1 -eq 6 ]
then
  echo "Bé."
  exit 0 
elif [ $1 -le 8 ]
then 
  echo "Notable"
  exit 0
elif [ $1 -le 10 ]
then
  echo "Excelent."
  exit 0
fi
 
