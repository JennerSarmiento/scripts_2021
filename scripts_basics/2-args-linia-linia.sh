#! /bin/bash
# @JennerSarmiento
# Febrer 2022
# Descripció: Arguments rebuts linia per linia enumerats

# Control N.Args
ERROR_NARGS=1
if [ $# -eq 0 ];
then
  echo "Error: Num args es igual a 0"
  echo "Usage: $0 Args ..."
  exit $ERROR_NARGS
fi

# Script
contador=0

for arg in $*
do
  ((contador++))
  echo "$contador: $arg"
done
exit 0
