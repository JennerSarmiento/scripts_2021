#! /bin/bash
# @JennerSarmiento
# Març de 2022
# Descripció: Validar si existeix un file. Per cada arg fer un comprimit. 
#	      Generar per stdout si l'ha comprimit o error per stderr si no es comprimeix.
#	      Al finalitzar mostrar per stdout quantes ha comprimit correctament.
#	      STATUS=0 ERROR_NARGS=1 ERROR_COMPRIMIR=2
#	      Amplia el programa amb un -h --help
#--------------------------------------------------------------------------------------------
STATUS=0
ERROR_NARGS=1
ERROR_COMPRIMIR=2

#Validar el numero d'args
if [ $# -eq 0 ]
then
  echo "Error, no hi ha arguments"
  echo "Usage: $0 file..."
  exit $ERROR_NARGS
fi

#Xixa
if [ $1 = "-h" -o $1 = "--help" ]
then
  echo "Ajuda del Programa @JennerSarmiento"
  echo "USAGE: $0 file [...]"
  exit 0
fi

for file in $*
do
  if [ -f $file ]
  then
    gzip $file &> /dev/null
    if [ $? -eq 0 ]
    then
      echo "$file comprimit."
      ((COMPTADOR++))
    else
      echo "Error a la hora de comprimir $arg." >> /dev/stderr
      exit $ERROR_COMPRIMIR
    fi
  fi
done
echo "Número de fitxers comprimits: $COMPTADOR."
