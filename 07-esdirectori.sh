#! /bin/bash
# @JennerSarmiento
# llistar el directori rebut
# -------------------------------
ERR_ARGS=1
ERR_NODIR=2
# Error N.Args
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_ARGS
fi
# Error Dir
if [ ! -d $1  ]; then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi	

# Script
dir=$1
ls $dir
exit 0
