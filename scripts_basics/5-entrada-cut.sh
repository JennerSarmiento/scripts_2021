#! /bin/bash
# @JennerSarmiento
# Febrer 2022
# Descripció: Mostrar per entrada estandar retallant els 50 primers caracters
#-----------------------------------------------------------------------------
#Script
while read -r line
do 
  echo "$line" | cut -c 1-50 
done
