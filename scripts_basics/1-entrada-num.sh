#! /bin/bash
# @JennerSarmiento
# Febrer 2022
# Descripció: Entrada estàndar numerat

# Script
contador=0
while read -r line
do
 ((contador++))
 echo "$contador:  $line"
done
exit 0
