#! /bin/bash
# Autor: @JennerSarmiento
# Any: 2021
# 02-exemples.sh
# Descripció: Processar arguments
#-------------------------------------------------------------
echo '$*: ' $*
echo '$@: ' $@
echo '$#: ' $#
echo '$0: ' $0
echo '$1: ' $1
echo '$2: ' $2
echo '$10: ' ${10}
echo '$11: ' ${11}
echo '$$: ' $$
nom="puig"
echo "${nom}delworld"
exit 0


